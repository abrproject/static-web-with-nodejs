const express = require('express');
const app = express();
const path = require('path');

//Settings
app.set('port', process.env.PORT || 8080);

//Static Files
app.use(express.static(path.join(__dirname, 'public')));

//Start Server
app.listen(app.get('port'), (req, res) => {
  console.log(`Server running on port ${app.get('port')}`);
})
